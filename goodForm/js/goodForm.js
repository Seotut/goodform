$(function() {


// Часть скипта для всплывающей формы
//var template = $('#vspl-form-template').html();

//$('body').append(template);

$("#dialog").dialog({modal:true, autoOpen: false, show: {effect: 'bounce', duration: 600}, minWidth: 200, minHeight: 200, closeText: '', title: "Ошибка!",  resizable: false,
buttons: [{text: "OK", click: function() {$(this).dialog("close")}}], closeOnEscape: true
 });

var form = $('#good-form-vspl');
var content = $(form).find('.formContent');

//var append = $(form).find('.append');


$('.good-form-btn').on('click',
function () {
//console.log('Всплывающая форма');

var type = $(this).attr('data-type');

$(form).find('input[name="type"]').val(type);

window.type = type;

// Автоматически определяется положение формы
var y = $(document).scrollTop() + 150;
var x = ($(document).width() - $(form).outerWidth())/2;

$(form).css({left: x, top: y});

$(form).fadeIn(350);

$('.good-form-overlay').fadeIn(350);

$(".good-form-overlay, #good-form-vspl .close").on('click',
function() {
$(".good-form-overlay, #good-form-vspl").fadeOut(350);

$(form).find('span.info').hide().html('');

}
);

//При изменении значения

$(form).find('input:not([type="submit"])').blur(function() {

$(form).find('span.info').fadeOut(100).html('');    
    
});

return false;

}
);


$(content).submit( function (e) {

	e.preventDefault();


$(content).find('span.info').fadeOut(100).html('');

var values = $(this).serialize();

var type = window.type;


	var email = $(form).find('input[name="email"]').val().trim();

	var phone = $(form).find('input[name="phone"]').val().trim();


	 if (email.length < 5 && phone.length < 5 ) {

$(content).find('span.info').css('color', 'red').fadeIn(100).html('Укажите ваш e-mail или телефон в правильном формате.');

	return false;
		}



$.ajax({
type: 'POST',
dataType: 'json',
data: values,
url: "/themes/goodForm/php/goodForm.php",
success: function(msg) {

if (msg.error == 0)  {

//	Указать id счётчика здесь
//console.log(type);

// Достижение цели 
// Нужно раскомментировать и заменить XXXX на номер счётчика
// В достижение цели передаётся переменная type

//yaCounterXXXXXX.reachGoal(type);

$(form).find('input:not([type="submit"]), textarea').val('');

$(form).find('span.info').html('Спасибо! Ваше сообщение успешно отправлено.').css('color', 'green').fadeIn(300);

}

else {
$(form).find('span.info').html(msg.error).css('color', 'red').fadeIn(300);
}

},
error: function() {
$(form).find('span.info').html('Не удалось отправить письмо! Попробуйте позже.').css('color', 'red').fadeIn(300);
}
});

return false;

}
);


// Статичная форма
$('.good-form-static').submit(
function () {

	var values = $(this).serialize();

	var type =  $(this).find('input[name=type]').val();

	

$.ajax({
type: 'POST',
dataType: 'json',
url: "/themes/goodForm/php/goodForm.php",
data: values,
success: function(msg) {
//console.log('Результат:' + msg.error);

if (msg.error == 0)  {


$("#dialog" ).dialog( {title: "Форма отправлена!", show: {effect: 'drop', duration: 600} });

      $('#dialog').removeClass('fail').addClass('success').html('Спасибо! Ваше сообщение успешно отправлено!').dialog("open");

console.log(type);

// Достижение цели
// Нужно раскомментировать и заменить XXXX на номер счётчика
// В достижение цели передаётся переменная type.


//yaCounterXXXXXX.reachGoal(type);

$('.good-form-static').find('input:not(:submit, :button), textarea').val('');

//alert('Спасибо! Ваше сообщение успешно отправлено.');

}

else {
//alert(msg.error);
  $('#dialog').removeClass('success').addClass('fail').html(msg.error).dialog({'title': 'Ошибка!'});
  $('#dialog').dialog("open");

}

},
error: function() {
$(form).find('span.info').html('Не удалось отправить письмо! Попробуйте позже.').css('color', 'red').fadeIn(300);
}
});

return false;
}
	);


}
);