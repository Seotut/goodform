<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);

define('MODX_API_MODE', true);
// здесь прописывается путь до индексного файла modx. 
// зависит от местонахождения файла по отношению к modx
// если адрес указан неверно, форма отсылаться не будет

$path = $_SERVER['DOCUMENT_ROOT'] . '/index.php'; // Путь к индексному файлу. 
// В большинстве случаев при стандартной структуре папок должен определяться верно.

//echo $path;

require_once($path);

$values = $_POST; 

// Сохраняем в переменную $values 
//массив $_POST со всеми присланными значениями

$msg = array();

// В ассоциативном массиве formArr хранятся названия форм, которые зависят от её типа. Добавляем новые значения по желанию.

$formArr = array('zakaz' => 'Заказать', 'mail' => 'Написать письмо', 'zvonok' => 'Заказать звонок', 'consult' => 'Консультация специалиста', 'vopros' => 'Задать вопрос', 'zayavka' => 'Оставить заявку', 'review' => 'Оставить отзыв');

if (empty($values['name']) || (empty($values['email']) && empty($values['phone']))) { $msg['error'] = 'Ошибка! Вы не заполнили обязательные поля. Укажите ваше имя и телефон или e-mail';
exit(json_encode($msg));
}

else { // Если нужные значения переданы

$sitename = $modx->getOption('site_name'); // Название сайта

$site = $modx->getOption('site_url');

$url = $_SERVER['HTTP_REFERER']; // Адрес страницы, с которой обратились к скрипту.

$type = $values['type'];

$name = trim($values['name']);

if (!empty($values['phone'])) { $phone = trim($values['phone']);

if (!preg_match("/[0-9+-]{5,}/", $phone)) { 

$msg['error'] = 'Ошибка! Неправильный формат поля телефон.';
exit(json_encode($msg));

}

}

if (!empty($values['email'])) { 

$email = trim($values['email']);

if ( !filter_var($email, FILTER_VALIDATE_EMAIL) ) {
$msg['error'] = 'Ошибка! Укажите ваш e-mail в правильном формате!';
exit(json_encode($msg));
}

}

if (!empty($values['comment'])) $comment = trim($values['comment']);

$subject = "Сообщение с сайта \"$sitename\" от $name. Форма: " . $formArr[$type];
$message = "Имя: $name.\n";
if ($phone) $message .= "Телефон: $phone.\n";
if ($email) $message .= "E-mail: $email.\n";

if (isset($comment)) $message .= "Комментарий: $comment\n";

$message .= "Адрес страницы, с которой отправлена форма: $url\n";

// Адрес почты берём из настроек MODX.
// Задавать почту в файле отдельно не нужно
//$to = 'sinnerpocht@yandex.ru';
$to = $modx->getOption('emailsender');
//echo $to;

$headers = "Content-type: text/plain; charset=utf-8\r\n";

$headers .= "From: $sitename <$to>\r\n";
$headers .= "MIME-Version: 1.0\r\n"; 
$headers .= "Date: ". date('D, d M Y h:i:s O') ."\r\n"; 

//$headers .= "From: $to\r\n";
//$headers = 'From: '.$name;

if (mail ($to, $subject, $message, $headers))
$msg['error'] = 0;

else  $msg['error'] = "Ошибка! Не удалось отправить письмо. Попробуйте позже.";

} //

$msg = json_encode($msg);
exit($msg);